import React from 'react'
import { Route, Link } from 'react-router-dom'
import { Nav, Navbar, Container } from 'react-bootstrap';
import Home from '../home'
import Editor from '../editor'
import Login from '../login'


const logout = () => {
  localStorage.clear();
}

const App = () => (
  <div>
    <Navbar bg="dark" expand="lg" variant="dark">
      <Navbar.Brand href="/">Магазин.RU</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Link className='nav nav-link' to="/">Главная страница</Link>
          <Link className='nav nav-link' to="editor">Редактирование товаров</Link>  
        </Nav>
        {localStorage.access_token &&
          <Nav pullright="true">
            <Link className='nav nav-link' onClick={logout} to="login">Выйти</Link>
          </Nav>
        }
        {!localStorage.access_token &&

          <Nav pullright="true">
            <Link className='nav nav-link' to="login">Войти</Link>
          </Nav>
        }
      </Navbar.Collapse>
    </Navbar>
    <Container fluid="sm">
      <Route exact path="/" component={Home} />
      <Route exact path="/editor" component={Editor} />
      <Route exact path="/login" component={Login} />
    </Container>
  </div>
)

export default App
