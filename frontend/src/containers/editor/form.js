import React from 'react'
import { Container, Form, Button, Modal } from 'react-bootstrap'
import { emptyFn } from '../../utils'

export default class EditorForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ...props,
            controlId: {
                name: `name-${this.props.id}`,
                description: `description-${this.props.id}`,
                price: `price-${this.props.id}`
            },
            showDeleteModal: false
        }

        this.state.name = this.state.name || ''
        this.state.description = this.state.description || ''
        this.state.price = this.state.price || 0.00
    }

    onSubmit = (e) => {
        e.preventDefault();

        const { id, name, description, price } = this.state;

        const onSubmit = this.props.onSubmit || emptyFn;

        onSubmit({ id, name, description, price });

    }

    onDelete = (e) => {
        e.preventDefault();

        const { id } = this.state;

        const onDelete = this.props.onDelete || emptyFn;

        onDelete(id);
    }

    onShowDeleteModal = (e) => {
        e.preventDefault();
        this.setState({showDeleteModal: true})
    }

    onCancelDelete = (e) => {
        e.preventDefault();
        this.setState({showDeleteModal: false})
    }

    onChange = (e) => {

        if (e.target.pattern && !RegExp(e.target.pattern).test(e.target.value)) {
            return;
        }

        if (e.target.name) {
            window.ttt = e.target
            const value = e.target.name === 'price'
                ? Number.parseFloat(e.target.value)
                : e.target.value;
            this.setState({ [e.target.name]: value })
        }

    }

    renderModal() {
        return (
            <div>
                <Modal show={this.state.showDeleteModal} onHide={this.onCancelDelete}>
                    <Modal.Header closeButton>
                        <Modal.Title>Удаление записи</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Вы действительно хотите удалить запись?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.onCancelDelete}>
                            Отмена
                        </Button>
                        <Button variant="primary" onClick={this.onDelete}>
                            Удалить
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }

    render() {
        const controlId = this.state.controlId;
        return (
            <Container>
                <Form>
                    <Form.Group controlId={controlId.name}>
                        <Form.Label>Наименование товара</Form.Label>
                        <Form.Control size="sm" name="name" value={this.state.name} onChange={this.onChange} />
                    </Form.Group>

                    <Form.Group controlId={controlId.description}>
                        <Form.Label>Описание товара</Form.Label>
                        <Form.Control size="sm" as="textarea" rows="3" name="description" value={this.state.description} onChange={this.onChange} />
                    </Form.Group>

                    <Form.Group controlId={controlId.price}>
                        <Form.Label>Цена</Form.Label>
                        <Form.Control size="sm" value={this.state.price} name="price" pattern="^\$?[\d,]+(\.\d{0,2})?$" onChange={this.onChange} />
                    </Form.Group>

                    <Button variant="primary" type="submit" onClick={this.onSubmit}>
                        {this.props.submitButtonText || 'Сохранить'}
                    </Button>
                    {'  '}
                    {this.state.id && <Button variant="danger" onClick={this.onShowDeleteModal}>
                        {'Удалить'}
                    </Button>}
                </Form>
                {this.renderModal()}
            </Container>
        )
    }
}