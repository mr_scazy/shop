import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Container, Pagination, Alert } from 'react-bootstrap'
import { query, update, save, remove, authorized } from '../../modules/editor'
import EditorForm from './form'
import { Redirect } from 'react-router'

const TAKE = 10;

class Editor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            take: TAKE
        }
    }

    componentDidMount() {
        if (!this.props.isInitialized) {
            this.reload()
        }
    }

    componentDidUpdate(prevProps) {

        if (this.props.isInitialized !== prevProps.isInitialized) {
            this.reload();
        }
    }

    reload = () => {
        var params = {
            page: this.state.page,
            take: this.state.take,
        }

        this.props.query(params)
    }

    handlePageItemClick = (e) => {
        e.preventDefault();
        if (!e.target.text) {
            return;
        }

        this.props.query({
            page: e.target.text,
            take: this.state.take
        })
        this.setState({ page: e.target.text })
    }

    onUpdate = (params) => {
        console.log(params)
        this.props.update(params)
    }

    onSave = (params) => {
        console.log(params)
        this.props.save(params)
    }

    onDelete = (id) => {
        console.log(id)
        this.props.remove(id)
    }    

    renderProduct(item) {
        return (
            <div key={item.id}>
                <EditorForm {...item} submitButtonText="Изминить" onSubmit={this.onUpdate} onDelete={this.onDelete} />
                <hr /><br />
            </div>
        )
    }

    renderPagination() {

        const items = []

        const { page, take } = this.state;

        let total = this.props.data.total;

        let currentPage = 0;

        do {
            currentPage++;
            items.push(
                <Pagination.Item
                    key={currentPage}
                    active={currentPage.toString() === page.toString()}
                    onClick={this.handlePageItemClick.bind(this)} >
                    {currentPage}
                </Pagination.Item>
            )

            total = total - take

        } while (total > 0)

        return (
            <Pagination>
                {items}
            </Pagination>
        )
    }

    render() {
        const items = this.props.data.items || []

        if (!this.props.isAuthorized || !localStorage.access_token) {
            return <Redirect to="/login" />
        }

        return (
            <div>
                <br /><h2>Редактирование товаров</h2><br />
                <Container>
                    {this.props.error && <Alert variant="danger">{this.props.error}</Alert>}
                    <h4>Создание товара</h4>
                    <EditorForm submitButtonText="Создать" onSubmit={this.onSave} />
                    <hr /><br />
                    <h4>Список товаров</h4>
                    {items.map(item => this.renderProduct(item))}
                    {this.renderPagination()}
                </Container>
            </div>
        )
    }
}



const mapStateToProps = ({ editor }) => ({
    data: editor.data,
    error: editor.error,
    params: editor.params,
    isAuthorized: editor.isAuthorized,
    isFetching: editor.isFetching,
    isInitialized: editor.isInitialized
})

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            query,
            update,
            save,
            remove,
            authorized
        },
        dispatch
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Editor)
