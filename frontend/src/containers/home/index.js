import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Container, Row, Col, Badge, Pagination } from 'react-bootstrap'
import { query, resetInitialized } from '../../modules/product'

const TAKE = 10;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      take: TAKE
    }
  }

  componentDidMount() {
    if (!this.props.isInitialized) {
      var params = {
        page: this.state.page,
        take: this.state.take,
      }

      this.props.query(params)
    }
  }

  handlePageItemClick = (e) => {
    e.preventDefault();
    if (!e.target.text) {
      return;
    }

    this.props.query({
      page: e.target.text,
      take: this.state.take
    })
    this.setState({ page: e.target.text })
  }

  renderProduct(item) {
    return (
      <div key={item.id}>
        <Row>
          <Col><h5 style={{ color: '#007bff' }}>{item.name}</h5></Col>
          <Col xs={5}>{item.description}</Col>
          <Col><Badge pill variant="success"><h4>₽ {item.price}</h4></Badge></Col>
        </Row>
        <br />
      </div>
    )
  }

  renderPagination() {

    const items = []

    const { page, take } = this.state;

    let total = this.props.data.total;

    let currentPage = 0;

    do {
      currentPage++;
      items.push(
        <Pagination.Item
          key={currentPage}
          active={currentPage.toString() === page.toString()}
          onClick={this.handlePageItemClick.bind(this)} >
          {currentPage}
        </Pagination.Item>
      )

      total = total - take

    } while (total > 0)

    return (
      <Pagination>
        {items}
      </Pagination>
    )
  }

  render() {

    const items = this.props.data.items || []

    return (
      <div>
        <br /><h2>Список товаров</h2><br />
        <Container>
          {items.map(item => this.renderProduct(item))}
          {this.renderPagination()}
        </Container>
      </div>
    )
  }
}

const mapStateToProps = ({ product }) => ({
  data: product.data,
  params: product.params,
  isFetching: product.isFetching,
  isInitialized: product.isInitialized
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      query,
      resetInitialized
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
