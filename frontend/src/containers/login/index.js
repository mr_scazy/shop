import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Alert, Form, Button, Container } from 'react-bootstrap';
import { authorized } from '../../modules/editor'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class Login extends Component {
  displayName = Login.name

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: null,
      redirect: false
    };

    if (localStorage.access_token) {
      this.state = { ...this.state, redirect: true }
    }
  }

  onSubmit = (e) => {
    e.preventDefault();

    const unEl = document.getElementById('username');
    const pwEl = document.getElementById('password');

    const formData = new URLSearchParams();

    formData.append('grant_type', 'password');
    formData.append('username', unEl.value);
    formData.append('password', pwEl.value);
    formData.append('scope', 'openid');
    formData.append('client_id', 'ro.shop');
    formData.append('client_secret', 'secret');

    let options = {
      method: 'POST',
      body: formData.toString(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };

    fetch(`identity/connect/token`, options)
      .then(response => response.json())
      .then(result => {
        if (result.access_token) {
          this.setData(result);
        } else if (result.error === 'invalid_grant') {
          this.setState({ error: 'Неверный логин или пароль' })
        } else {
          this.setState({ error: 'Произошла ошибка' })
        }

      })
      .catch(_ => this.setState({ error: 'Произошла ошибка' }));
  }

  setData = (data) => {
    localStorage.setItem('access_token', data.access_token);
    this.props.authorized(true)
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }

    return (
      <Container>
        <br/><br/>
        <Form onSubmit={this.onSubmit}>
          <Form.Group controlId="username">
            <Form.Label>Логин</Form.Label>
            <Form.Control type="text" placeholder="Введите логин" />            
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Пароль</Form.Label>
            <Form.Control type="password" placeholder="Пароль" />
          </Form.Group>

          <Button variant="primary" type="submit">Войти</Button>
        </Form><br/>
        {this.state.error && <Alert variant="danger">{this.state.error}</Alert>}
      </Container>
    )
  }
}

const mapStateToProps = ({ editor }) => ({
  isAuthorized: editor.isAuthorized
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authorized
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)