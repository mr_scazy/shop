import { fetchGet, fetchPost, fetchPut, fetchDelete } from '../utils'
import { push } from 'connected-react-router'
import { RESET_INITIALIZED as PRODUCT_RESET_INITIALIZED } from './product'

export const GET = 'editor/GET'
export const GET_SUCCEED = 'editor/GET_SUCCEED'
export const GET_FAILED = 'editor/GET_FAILED'
export const RESET_INITIALIZED = 'editor/GET_RESET_INITIALIZED'
export const AUTORIZED = 'editor/AUTORIZED'
export const UNAUTORIZED = 'editor/UNAUTORIZED'

const initialState = {
    data: {},
    params: {},
    error: '',
    isAuthorized: true,
    isFetching: false,
    isInitialized: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET:
            return {
                ...state,
                params: action.params,
                isFetching: true
            }

        case GET_SUCCEED:
            return {
                ...state,
                data: action.payload.data,
                isFetching: false,
                isInitialized: true
            }

        case GET_FAILED:
            return {
                ...state,
                error: action.payload.data && action.payload.data.message,
                isFetching: false,
                isInitialized: false
            }

        case RESET_INITIALIZED:
            return {
                ...state,
                isInitialized: false
            }

        case AUTORIZED:
            return {
                ...state,
                isAuthorized: true
            }

        case UNAUTORIZED:
            return {
                ...state,
                isAuthorized: false
            }

        default:
            return state
    }
}

export const authorized = (value = false) => {
    return dispatch => {
        dispatch({
            type: value ? AUTORIZED : UNAUTORIZED
        })
    }
}

export const resetInitialized = () => {
    return dispatch => {
        dispatch({
            type: RESET_INITIALIZED
        })
    }
}

export const checkStatusCode = (response) => {

    if (response.status === 401) {
        console.log(response)
        localStorage.clear()
        push('login')
        return { errorCode: 401, message: 'Неавторизованный пользователь' }
    }
    return response.json()
}

const joinErrors = (errors, delimiter = ' ') => {
    let message = '';

    if(!errors) {
        return message;
    }

    for (var prop in errors) {
        message = message + delimiter + errors[prop]
    }
    
    return message;
}

export const query = (params) => {
    return dispatch => {

        params = params || {}
        params.page = params.page || 1
        params.take = params.take || 10

        dispatch({
            type: GET,
            params
        })

        fetchGet(`api/v1/editor/product?page=${params.page}&take=${params.take}`)
            .then(checkStatusCode)
            .then(result => {
                if (result.errorCode === 0) {

                    dispatch({
                        type: GET_SUCCEED,
                        payload: { data: result }
                    });
                } else if (result.errorCode === 401) {
                    dispatch({
                        type: UNAUTORIZED
                    });
                } else {
                    dispatch({
                        type: GET_FAILED,
                        payload: { data: result }
                    });
                }
            })
            .catch(e => {
                dispatch({
                    type: GET_FAILED,
                    payload: {
                        data: {
                            errorCode: 500,
                            message: `Произошла ошибка. ${e.message}`
                        }
                    }
                });
            });
    }
}

export const update = (params) => {
    return dispatch => {

        fetchPut(`api/v1/editor/product/${params.id}`, params)
            .then(checkStatusCode)
            .then(result => {
                if (result.errorCode === 0) {
                    dispatch({ type: RESET_INITIALIZED });
                    dispatch({ type: PRODUCT_RESET_INITIALIZED })
                } else if (result.errorCode === 401) {
                    dispatch({
                        type: UNAUTORIZED
                    });
                } else if (result.status === 400) {
                    dispatch({
                        type: GET_FAILED,
                        payload: { 
                            data: {
                                errorCode: result.status,
                                message: joinErrors(result.errors)
                            } 
                        }
                    });
                } else {
                    dispatch({
                        type: GET_FAILED,
                        payload: { data: result }
                    });
                }
            })
            .catch(e => {
                dispatch({
                    type: GET_FAILED,
                    payload: {
                        data: {
                            errorCode: 500,
                            message: `Произошла ошибка. ${e.message}`
                        }
                    }
                });
            });
    }
}

export const save = (params) => {
    return dispatch => {

        fetchPost(`api/v1/editor/product`, params)
            .then(checkStatusCode)
            .then(result => {
                if (result.errorCode === 0) {
                    dispatch({ type: RESET_INITIALIZED });
                    dispatch({ type: PRODUCT_RESET_INITIALIZED })
                } else if (result.errorCode === 401) {
                    dispatch({
                        type: UNAUTORIZED
                    });
                } else if (result.status === 400) {
                    dispatch({
                        type: GET_FAILED,
                        payload: { 
                            data: {
                                errorCode: result.status,
                                message: joinErrors(result.errors)
                            } 
                        }
                    });
                } else {
                    dispatch({
                        type: GET_FAILED,
                        payload: { data: result }
                    });
                }
            })
            .catch(e => {
                dispatch({
                    type: GET_FAILED,
                    payload: {
                        data: {
                            errorCode: 500,
                            message: `Произошла ошибка. ${e.message}`
                        }
                    }
                });
            });
    }
}

export const remove = (id) => {
    return dispatch => {

        fetchDelete(`api/v1/editor/product/${id}`)
            .then(checkStatusCode)
            .then(result => {
                if (result.errorCode === 0) {
                    dispatch({ type: RESET_INITIALIZED });
                    dispatch({ type: PRODUCT_RESET_INITIALIZED })
                } else if (result.errorCode === 401) {
                    dispatch({
                        type: UNAUTORIZED
                    });
                } else {
                    dispatch({
                        type: GET_FAILED,
                        payload: { data: result }
                    });
                }
            })
            .catch(e => {
                dispatch({
                    type: GET_FAILED,
                    payload: {
                        data: {
                            errorCode: 500,
                            message: `Произошла ошибка. ${e.message}`
                        }
                    }
                });
            });
    }
}