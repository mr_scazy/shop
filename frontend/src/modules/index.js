import { combineReducers } from 'redux'
import product from './product'
import editor from './editor'

export default combineReducers({
  product,
  editor
})
