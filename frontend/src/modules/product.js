import { fetchGet } from '../utils'

export const GET = 'product/GET'
export const SUCCEED = 'product/GET_SUCCEED'
export const FAILED = 'product/GET_FAILED'
export const RESET_INITIALIZED = 'product/GET_RESET_INITIALIZED'

const initialState = {
    data: {},
    params: {},
    isFetching: false,
    isInitialized: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET:
            return {
                ...state,
                params: action.params,
                isFetching: true
            }

        case SUCCEED:
            return {
                ...state,
                data: action.payload.data,
                isFetching: false,
                isInitialized: true
            }

        case FAILED:
            return {
                ...state,
                error: action.payload.data && action.payload.data.message,
                isFetching: false,
                isInitialized: true
            }

        case RESET_INITIALIZED:
            return {
                ...state,
                isInitialized: false
            }

        default:
            return state
    }
}

export const resetInitialized = () => {
    return dispatch => {
        dispatch({
            type: RESET_INITIALIZED
        })
    }
}

export const query = (params) => {
    return dispatch => {

        params = params || {}
        params.page = params.page || 1
        params.take = params.take || 10

        dispatch({
            type: GET,
            params
        })

        fetchGet(`api/v1/product?page=${params.page}&take=${params.take}`)
            .then(response => response.json())
            .then(result => {
                if (result.errorCode === 0) {

                    dispatch({
                        type: SUCCEED,
                        payload: { data: result }
                    });
                } else {
                    dispatch({
                        type: FAILED,
                        payload: { data: result }
                    });
                }
            })
            .catch(e => {
                dispatch({
                    type: FAILED,
                    payload: {
                        data: {
                            errorCode: 500,
                            message: `Произошла ошибка. ${e.message}`
                        }
                    }
                });
            });
    }
}
