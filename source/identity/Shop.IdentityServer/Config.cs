using System.Collections.Generic;
using System.Security.Claims;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace Shop.IdentityServer
{
    /// <summary>
    /// Конфигурации IdentityServer
    /// </summary>
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile()
            };
        }

        /// <summary>
        /// Получить список <see cref="ApiResource"/>
        /// </summary>
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("shop", "Shop API")
            };
        }

        /// <summary>
        /// Получить списко клиентов
        /// </summary>
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "ro.shop",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.Address
                    }
                }
            };
        }

        /// <summary>
        /// Получить тестовых пользователей
        /// </summary>
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "john",
                    Password = "password",

                    Claims = new List<Claim>
                    {
                        new Claim("name", "John")
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "jane",
                    Password = "password",

                    Claims = new List<Claim>
                    {
                        new Claim("name", "Jane")
                    }
                }
            };
        }
    }
}