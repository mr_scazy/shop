namespace Shop.IdentityServer
{
    /// <summary>
    /// Секция конфигурации IdentityServer
    /// </summary>
    public class IdentityServerSection
    {
        /// <summary>
        /// URL эмитета
        /// </summary>
        public string IssuerUri { get; set; }
    }
}