using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Shop.Api.Controllers;

namespace Shop.Api.Attributes
{
    /// <summary>
    /// Фильтр <see cref="ApiResponse"/> для выставления HTTP-кода
    /// </summary>
    public class ApiResponseFilterAttribute : ActionFilterAttribute
    {
        /// <inheritdoc cref="ActionFilterAttribute.OnActionExecuted"/>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var httpContext = context.HttpContext;
            var response = httpContext.Response;
            if (response == null)
            {
                base.OnActionExecuted(context);
                return;
            }

            if (context.Result is ObjectResult result &&
                result.Value is ApiResponse apiResponse &&
                apiResponse.ErrorCode != 0)
            {
                var httpStatusCode = apiResponse.ErrorCode switch
                {
                    500 => HttpStatusCode.InternalServerError,
                    400 => HttpStatusCode.BadRequest,
                    _ => (HttpStatusCode) 422
                };

                httpContext.Response.StatusCode = (int) httpStatusCode;
            }

            base.OnActionExecuted(context);
        }
    }
}