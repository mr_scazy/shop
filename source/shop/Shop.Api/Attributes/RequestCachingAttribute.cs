using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Shop.Core.Extensions;
using Shop.Service;

namespace Shop.Api.Attributes
{
    /// <summary>
    /// Атрибут для кэширования ответов на запросы
    /// </summary>
    public class RequestCachingAttribute : ActionFilterAttribute
    {
        private const string RequestCachingKey = "request_caching_key";
        
        /// <summary>
        /// Ключ экшена
        /// </summary>
        public string ActionKey { get; set; }

        /// <summary>
        /// Атрибут для кэширования ответов на запросы
        /// </summary>
        /// <param name="actionKey">Ключ экшена</param>
        public RequestCachingAttribute(string actionKey)
        {
            ActionKey = actionKey;
        }

        /// <inheritdoc cref="ActionFilterAttribute.OnActionExecutionAsync"/>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }

            await TrySetResultFromCacheAsync(context);

            OnActionExecuting(context);
            if (context.Result == null)
            {
                OnActionExecuted(await next());
            }
        }

        /// <inheritdoc cref="ActionFilterAttribute.OnResultExecutionAsync"/>
        public override async Task OnResultExecutionAsync(
            ResultExecutingContext context,
            ResultExecutionDelegate next)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }

            OnResultExecuting(context);
            if (!context.Cancel)
            {
                OnResultExecuted(await next());
                
                await TryAddResultToCacheAsync(context);
            }
        }

        private async Task TrySetResultFromCacheAsync(ActionExecutingContext context)
        {
            var cachingService = context.HttpContext.RequestServices.GetService<IRequestCachingService>();

            var json = JsonConvert.SerializeObject(context.ActionArguments);
            
            var requestKey = json.GetSha256Hash();

            var value = await cachingService.TryGetResultAsync(ActionKey, requestKey);

            if (value is IActionResult result)
            {
                context.Result = result;
            }
            else
            {
                context.HttpContext.Items.Add(RequestCachingKey, requestKey);
            }
        }
        
        private async Task TryAddResultToCacheAsync(ResultExecutingContext context)
        {
            if (!(context.Result is ObjectResult))
            {
                return;
            }
            
            var statusCode = context.HttpContext.Response?.StatusCode;
            if (statusCode >= 200 && statusCode < 300)
            {
                var items = context.HttpContext.Items;

                items.TryGetValue(RequestCachingKey, out var requestKeyObject);

                if (requestKeyObject is string requestKey)
                {
                    var cachingService = context.HttpContext.RequestServices.GetService<IRequestCachingService>();
                    await cachingService.AddResultAsync(ActionKey, requestKey, context.Result);
                }
            }
        }
    }
}