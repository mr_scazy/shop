using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Shop.Api.Controllers;
using Shop.Core.Enums;

namespace Shop.Api.Attributes
{
    /// <summary>
    /// Фильтр необработанных исключений
    /// </summary>
    public class UnhandledExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private const string SystemError = "Произошла системная ошибка";

        /// <inheritdoc cref="ExceptionFilterAttribute.OnException"/>
        public override void OnException(ExceptionContext context)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILogger<UnhandledExceptionFilterAttribute>>();
            
            logger?.LogError($"Unhandled exception: {context.Exception}");

            var response = new ApiResponse
            {
                Message = SystemError,
                ErrorCode = (int) ErrorCodes.SystemError
            };

            context.Result = new ObjectResult(response)
            {
                StatusCode = 500, 
                ContentTypes = {"application/json"}
            };

            context.ExceptionHandled = true;
        }
    }
}