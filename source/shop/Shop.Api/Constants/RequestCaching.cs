namespace Shop.Api.Constants
{
    /// <summary>
    /// Константы ключей для кэширования запросов
    /// </summary>
    public static class RequestCaching
    {
        public const string ProductGetItem = nameof(ProductGetItem);
        
        public const string ProductGetList = nameof(ProductGetList);
    }
}