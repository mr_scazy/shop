namespace Shop.Api.Constants
{
    public static class ValidationMessages
    {
        public const string Required = "Поле '{0}' обязательное.";

        public const string MaxLength = "Поле '{0}' должно быть строкой с максимальной длиной '{1}'.";
    }
}