namespace Shop.Api.Controllers
{
    /// <summary>
    /// Базовый API респонс
    /// </summary>
    public class ApiResponse
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        public int ErrorCode { get; set; }
        
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set;}
    }
}