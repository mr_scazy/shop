using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shop.Api.Constants;
using Shop.Api.Controllers.Private.V1.Requests;
using Shop.Api.Controllers.Private.V1.Responses;
using Shop.Api.Controllers.Public.V1.Requests;
using Shop.Api.Controllers.Public.V1.Responses;
using Shop.Api.Extensions;
using Shop.Service;

namespace Shop.Api.Controllers.Private.V1
{
    /// <summary>
    /// Контроллер редактирования товара
    /// </summary>
    [ApiController, Authorize]
    [Route("v1/editor/product")]
    public class ProductEditorController : ControllerBase
    {
        private readonly IProductService _productService;
        
        private readonly IRequestCachingService _requestCachingService;

        /// <summary>
        /// Ctor
        /// </summary>
        public ProductEditorController(IProductService productService, IRequestCachingService requestCachingService)
        {
            _productService = productService;
            _requestCachingService = requestCachingService;
        }
        
        [HttpGet("{id}")]
        public async Task<ApiResponse> Get(long id)
        {
            var result = await _productService.GetProductByIdAsync(id);
            
            return result.ToApiResponse(x => new ProductResponse
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                CreatedDate = x.CreatedDate
            });
        }
        
        [HttpGet]
        public async Task<ApiResponse> Get([FromQuery] ProductGetListRequest request)
        {
            var filter = request.ToFilter();

            var result = await _productService.GetProductListAsync(filter);
            
            return result.ToApiResponse(x => new ProductGetListResponse
            {
                Items = x.Items
                    .Select(z => new ProductItem
                    {
                        Id = z.Id,
                        Name = z.Name,
                        Description = z.Description,
                        Price = z.Price,
                        CreatedDate = z.CreatedDate
                    })
                    .ToArray(),
                Total = x.Total
            });
        }

        [HttpPost]
        public async Task<ApiResponse> Post([FromBody] ProductRequest request)
        {
            var dto = request.ToDto();

            var result = await _productService.AddProductAsync(dto);
            
            if (!result.HasError)
            {
                await _requestCachingService.ClearAsync(
                    RequestCaching.ProductGetItem, 
                    RequestCaching.ProductGetList);
            }

            return result.ToApiResponse(id => new ProductCreateResponse {Id = id});
        }

        [HttpPut("{id}")]
        public async Task<ApiResponse> Put(long id, [FromBody] ProductRequest request)
        {
            var dto = request.ToDto(id);
            
            var result = await _productService.UpdateProductAsync(dto);
            
            if (!result.HasError)
            {
                await _requestCachingService.ClearAsync(
                    RequestCaching.ProductGetItem, 
                    RequestCaching.ProductGetList);
            }
            
            return result.ToApiResponse();
        }
        
        [HttpDelete("{id}")]
        public async Task<ApiResponse> Delete(long id)
        {
            var result = await _productService.DeleteProductAsync(id);

            if (!result.HasError)
            {
                await _requestCachingService.ClearAsync(
                    RequestCaching.ProductGetItem, 
                    RequestCaching.ProductGetList);
            }

            return result.ToApiResponse();
        }
    }
}