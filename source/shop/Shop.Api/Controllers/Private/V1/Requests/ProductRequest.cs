using System.ComponentModel.DataAnnotations;
using Shop.Api.Constants;

namespace Shop.Api.Controllers.Private.V1.Requests
{
    /// <summary>
    /// Запрос продукта
    /// </summary>
    public class ProductRequest
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.Required)]
        [MaxLength(200, ErrorMessage = ValidationMessages.MaxLength)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        
        /// <summary>
        /// Описание
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.Required)]
        [MaxLength(1000, ErrorMessage = ValidationMessages.MaxLength)]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        
        /// <summary>
        /// Цена
        /// </summary>
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
    }
}