namespace Shop.Api.Controllers.Private.V1.Responses
{
    /// <summary>
    /// Ответ создания товара
    /// </summary>
    public class ProductCreateResponse : ApiResponse
    {
        /// <summary>
        /// Идентификатор созданного товара
        /// </summary>
        public long Id { get; set; }
    }
}