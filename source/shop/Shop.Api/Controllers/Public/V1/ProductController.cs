using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shop.Api.Attributes;
using Shop.Api.Constants;
using Shop.Api.Controllers.Public.V1.Requests;
using Shop.Api.Controllers.Public.V1.Responses;
using Shop.Api.Extensions;
using Shop.Service;

namespace Shop.Api.Controllers.Public.V1
{
    /// <summary>
    /// Контроллер товара
    /// </summary>
    [ApiController]
    [Route("v1/[controller]"), AllowAnonymous]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        /// <summary>
        /// Ctor
        /// </summary>
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("{id}"), RequestCaching(RequestCaching.ProductGetItem)]
        public async Task<ApiResponse> Get(long id)
        {
            var result = await _productService.GetProductByIdAsync(id);

            return result.ToApiResponse(x => new ProductResponse
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                CreatedDate = x.CreatedDate
            });
        }

        [HttpGet, RequestCaching(RequestCaching.ProductGetItem)]
        public async Task<ApiResponse> Get([FromQuery] ProductGetListRequest request)
        {
            var filter = request.ToFilter();

            var result = await _productService.GetProductListAsync(filter);
            
            return result.ToApiResponse(x => new ProductGetListResponse
            {
                Items = x.Items
                    .Select(z => new ProductItem
                    {
                        Id = z.Id,
                        Name = z.Name,
                        Description = z.Description,
                        Price = z.Price,
                        CreatedDate = z.CreatedDate
                    })
                    .ToArray(),
                Total = x.Total
            });
        }
    }
}