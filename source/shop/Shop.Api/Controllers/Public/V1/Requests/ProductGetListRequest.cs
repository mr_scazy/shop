namespace Shop.Api.Controllers.Public.V1.Requests
{
    /// <summary>
    /// Запрос для получения списка товаров
    /// </summary>
    public class ProductGetListRequest
    {
        /// <summary>
        /// Страница
        /// </summary>
        public int Page { get; set; } = 1;
        
        /// <summary>
        /// Число записей на странице
        /// </summary>
        public int Take { get; set; } = 20;
    }
}