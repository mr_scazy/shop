using System;
using System.Collections.Generic;

namespace Shop.Api.Controllers.Public.V1.Responses
{
    /// <summary>
    /// Ответ получения списка элементов
    /// </summary>
    public class ProductGetListResponse : ApiResponse
    {
        /// <summary>
        /// Элементы продукта
        /// </summary>
        public IReadOnlyList<ProductItem> Items { get; set; } = Array.Empty<ProductItem>();
        
        /// <summary>
        /// Общее количество элементов
        /// </summary>
        public int Total  { get; set; }
    }
}