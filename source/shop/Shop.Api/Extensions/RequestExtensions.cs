using System;
using Shop.Api.Controllers.Private.V1.Requests;
using Shop.Api.Controllers.Public.V1.Requests;
using Shop.Service.DTO;

namespace Shop.Api.Extensions
{
    /// <summary>
    /// Методы-расширения для запросов
    /// </summary>
    public static class RequestExtensions
    {
        /// <summary>
        /// Преобразовать в <see cref="ProductListFilter"/>
        /// </summary>
        public static ProductListFilter ToFilter(this ProductGetListRequest request) => new ProductListFilter
        {
            Page = request.Page,
            Take = request.Take
        };
        
        /// <summary>
        /// Преобразовать в <see cref="ProductDto"/>
        /// </summary>
        public static ProductDto ToDto(this ProductRequest request, long id = 0L) => new ProductDto
        {
            Id = id,
            Name = request.Name,
            Description = request.Description,
            Price = request.Price,
            CreatedDate = DateTime.UtcNow
        };
    }
}