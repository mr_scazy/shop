using System;
using System.Linq;
using Shop.Api.Controllers;
using Shop.Api.Controllers.Public.V1.Responses;
using Shop.Core.Results;
using Shop.Service.DTO;

namespace Shop.Api.Extensions
{
    /// <summary>
    /// Методы-расширения для ответов
    /// </summary>
    public static class ResponseExtensions
    {
        private const string NotFound = "Запись не найдена";
        
        /// <summary>
        /// Преобразовать в <see cref="ApiResponse"/>
        /// </summary>
        public static ApiResponse ToApiResponse(this IResult result) 
            => new ApiResponse
            {
                ErrorCode = (int) result.ErrorCode,
                Message = result.Message
            };

        /// <summary>
        /// Преобразовать в <see cref="ApiResponse"/>
        /// </summary>
        public static ApiResponse ToApiResponse<T>(this IResult<T> result, Func<T, ApiResponse> map)
        {
            result = result ?? throw new ArgumentNullException(nameof(result));
            
            map = map ?? throw new ArgumentNullException(nameof(map));
            
            if (result.HasError)
            {
                return result.ToApiResponse();
            }

            var response = map(result.Data);
            response.Message = result.Message;
            return response;
        }
    }
}