using System;
using System.Threading;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shop.Api.Attributes;
using Shop.Core.Configuration.Section;
using Shop.Data;
using Shop.Service;
using Shop.Service.Impl;

namespace Shop.Api
{
    /// <summary>
    /// Класс конфигурирования хоста
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ConnectionStringSection>(Configuration.GetSection("ConnectionString"));
            
            services.AddControllers(options =>
            {
                options.Filters.Add<ApiResponseFilterAttribute>();
                options.Filters.Add<UnhandledExceptionFilterAttribute>();
            });

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    var authSection = Configuration.GetSection("Authentication").Get<AuthenticationSection>();
                    
                    options.Authority = authSection.BaseUrl;
                    options.Audience = authSection.ApiName;
                    options.RequireHttpsMetadata = false;
                    
                    options.TokenValidationParameters.ValidateAudience = authSection.ValidateAudience;
                });

            services.AddScoped<IProductService, ProductService>();

            services.AddSingleton<IRequestCachingService, RequestCachingService>();

            services.AddDbContext<ShopDbContext>((provider,options) =>
            {
                var snapshot = provider.GetService<IOptionsSnapshot<ConnectionStringSection>>();
                options.UseNpgsql(snapshot.Value.ShopDb);
            });

            services.AddScoped<IUnitOfWork>(provider => provider.GetRequiredService<ShopDbContext>());
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(
            IApplicationBuilder app, 
            IWebHostEnvironment env, 
            IHostApplicationLifetime hostApplicationLifetime, 
            ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.CreateLogger("Startup");
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            hostApplicationLifetime.ApplicationStarted
                .Register(() => OnApplicationStarted(app, logger));

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private static void OnApplicationStarted(IApplicationBuilder app, ILogger logger)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_USE_AUTO_MIGRATE") == "1")
            {
                try
                {
                    Thread.Sleep(5000);
                    using var scope = app.ApplicationServices.CreateScope();
                    scope.ServiceProvider.GetRequiredService<ShopDbContext>().Database.Migrate();
                    logger.LogInformation("Database updated");
                }
                catch (Exception e)
                {
                    logger.LogCritical(e, "Database update error");
                }
            }
        }
    }
}