namespace Shop.Core.Configuration.Section
{
    public class AuthenticationSection
    {
        public string BaseUrl { get; set; }
        
        public string ApiName { get; set; }
        
        public bool ValidateAudience { get; set; }

    }
}