namespace Shop.Core.Configuration.Section
{
    /// <summary>
    /// Секция строки подключения
    /// </summary>
    public class ConnectionStringSection
    {
        /// <summary>
        /// Строка подключения к ShopDb
        /// </summary>
        public string ShopDb { get; set; }
    }
}