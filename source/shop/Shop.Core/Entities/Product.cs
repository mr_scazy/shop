using System;
using System.ComponentModel.DataAnnotations;

namespace Shop.Core.Entities
{
    /// <summary>
    /// Товар
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public long Id { get; set; }
        
        /// <summary>
        /// Наименование
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        [MaxLength(200)]
        public string Name { get; set; }
        
        /// <summary>
        /// Описание
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        [MaxLength(1000)]
        public string Description { get; set; }
        
        /// <summary>
        /// Цена
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}