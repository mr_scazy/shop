namespace Shop.Core.Enums
{
    /// <summary>
    /// Коды ошибок
    /// </summary>
    public enum ErrorCodes
    {
        /// <summary>
        /// Системная ошибка
        /// </summary>
        SystemError = 500,
        
        /// <summary>
        /// Ошибка валидации
        /// </summary>
        ValidationError = 400,
        
        /// <summary>
        /// Бизнесовая ошибка
        /// </summary>
        BusinessError = 422
    }
}