using System;
using System.Security.Cryptography;
using System.Text;

namespace Shop.Core.Extensions
{
    /// <summary>
    /// Методы-расширения для работы с криптографией
    /// </summary>
    public static class CryptographyExtensions
    {
        /// <summary>
        /// Получить хэш SHA256 строки
        /// </summary>
        /// <param name="source">Исходная строка</param>
        /// <returns>Хэш</returns>
        public static string GetSha256Hash(this string source)
        {
            source = source ?? throw new ArgumentNullException(nameof(source));

            using var md5 = SHA256.Create();
            
            var bytes = Encoding.UTF8.GetBytes(source);

            var hash = md5.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
    }
}