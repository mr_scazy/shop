using Shop.Core.Enums;

namespace Shop.Core.Results
{
    /// <summary>
    /// Результат
    /// </summary>
    public interface IResult
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        ErrorCodes ErrorCode { get; }
        
        /// <summary>
        /// Сообщение
        /// </summary>
        string Message { get; }
        
        /// <summary>
        /// Проверка на наличие ошибок
        /// </summary>
        bool HasError { get;  }
    }
    
    /// <summary>
    /// Результат c данными
    /// </summary>
    /// <typeparam name="T">Тип данных</typeparam>
    public interface IResult<out T> : IResult
    {
        /// <summary>
        /// Данные
        /// </summary>
        T Data { get; }
    }
}