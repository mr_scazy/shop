using Shop.Core.Enums;

namespace Shop.Core.Results
{
    /// <inheritdoc cref="IResult"/>
    public class Result : IResult
    {
        /// <inheritdoc cref="IResult.ErrorCode"/>
        public ErrorCodes ErrorCode { get; }
        
        /// <inheritdoc cref="IResult.Message"/>
        public string Message { get; }
        
        /// <inheritdoc cref="IResult.HasError"/>
        public bool HasError => ErrorCode > 0;

        /// <summary>
        /// Ctor
        /// </summary>
        protected Result(ErrorCodes errorCode, string message = null)
        {
            ErrorCode = errorCode;
            Message = message;
        }
        
        /// <summary>
        /// Создать неуспешный результат
        /// </summary>
        /// <param name="errorCode">Код ошибки</param>
        /// <param name="message">Сообщение</param>
        public static Result Failed(ErrorCodes errorCode, string message = null) 
            => new Result(errorCode, message); 
        
        /// <summary>
        /// Создать неуспешный результат
        /// </summary>
        /// <param name="errorCode">Код ошибки</param>
        /// <param name="message">Сообщение</param>
        /// <typeparam name="T">Тип данных</typeparam>
        public static Result<T> Failed<T>(ErrorCodes errorCode, string message = null) 
            => new Result<T>(default, errorCode, message); 
        
        /// <summary>
        /// Создать успешный результат
        /// </summary>
        /// <param name="message">Сообщение</param>
        public static Result Succeed(string message = null) 
            => new Result(0, message);
        
        /// <summary>
        /// Создать успешный результат с данными
        /// </summary>
        /// <param name="data">Данные</param>
        /// <param name="message">Сообщение</param>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <returns></returns>
        public static Result<T> FromData<T>(T data, string message = null) 
            => new Result<T>(data, 0, message);
    }

    /// <inheritdoc cref="IResult{T}"/>
    public class Result<T> : Result, IResult<T>
    {
        /// <inheritdoc cref="IResult{T}.Data"/>
        public T Data { get; }
        
        /// <summary>
        /// Ctor
        /// </summary>
        public Result(T data, ErrorCodes errorCode, string message = null) : base(errorCode, message)
        {
            Data = data;
        }
    }
}