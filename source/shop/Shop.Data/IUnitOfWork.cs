using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Shop.Data
{
    /// <summary>
    /// Интерфейс для работы с <see cref="DbContext"/>
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Получить <see cref="DbSet{TEntity}"/>
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        DbSet<TEntity> Set<TEntity>()
            where TEntity : class;

        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="cancellationToken">Токен отмены асинхронной операции</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        ValueTask<EntityEntry<TEntity>> AddAsync<TEntity>(
            [NotNull] TEntity entity,
            CancellationToken cancellationToken = default)
            where TEntity : class;

        /// <summary>
        /// Обновить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        EntityEntry<TEntity> Update<TEntity>([NotNull] TEntity entity)
            where TEntity : class;

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        EntityEntry<TEntity> Remove<TEntity>([NotNull] TEntity entity)
            where TEntity : class;
        
        /// <summary>
        /// Сохранить изменения
        /// </summary>
        /// <param name="cancellationToken">Токен отмены асинхронной операции</param>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}