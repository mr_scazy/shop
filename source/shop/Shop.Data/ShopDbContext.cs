using Microsoft.EntityFrameworkCore;
using Shop.Core.Entities;

namespace Shop.Data
{
    /// <summary>
    /// Контекст для работы с БД магазина
    /// </summary>
    public class ShopDbContext : DbContext, IUnitOfWork
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="options"></param>
        public ShopDbContext(DbContextOptions<ShopDbContext> options) : base(options)
        {
        }
        
        /// <summary>
        /// Продукты
        /// </summary>
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(x => x.Id);
            modelBuilder.Entity<Product>().HasIndex(x => x.CreatedDate);
            base.OnModelCreating(modelBuilder);
        }
    }
}