namespace Shop.Service.DTO
{
    /// <summary>
    /// Фильтр для получения списка товаров
    /// </summary>
    public class ProductListFilter
    {
        /// <summary>
        /// Страница
        /// </summary>
        public int Page { get; set; } = 1;
        
        /// <summary>
        /// Число записей на странице
        /// </summary>
        public int Take { get; set; } = 20;
    }
}