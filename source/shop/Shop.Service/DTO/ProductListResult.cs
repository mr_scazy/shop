using System;
using System.Collections.Generic;

namespace Shop.Service.DTO
{
    /// <summary>
    /// Результат со списоком продуктов
    /// </summary>
    public class ProductListResult
    {
        public IReadOnlyList<ProductDto> Items { get; set; } = Array.Empty<ProductDto>();
        
        public int Total  { get; set; }
    }
}