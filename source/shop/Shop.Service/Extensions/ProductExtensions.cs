using Shop.Core.Entities;
using Shop.Service.DTO;

namespace Shop.Service.Extensions
{
    /// <summary>
    /// Методы-расширения для <see cref="Product"/>
    /// </summary>
    public static class ProductExtensions
    {
        /// <summary>
        /// Преобразовать в <see cref="ProductDto"/>
        /// </summary>
        public static ProductDto ToDto(this Product product) => new ProductDto
        {
            Id = product.Id,
            Name = product.Name,
            Description = product.Description,
            Price = product.Price,
            CreatedDate = product.CreatedDate
        };
    }
}