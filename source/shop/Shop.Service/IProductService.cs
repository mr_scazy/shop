using System.Threading.Tasks;
using Shop.Core.Results;
using Shop.Service.DTO;

namespace Shop.Service
{
    /// <summary>
    /// Сервис для работы с продуктами
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Получить продукт по индентификатору
        /// </summary>
        /// <param name="id">Идентификатор продукта</param>
        /// <returns>Продукт</returns>
        Task<IResult<ProductDto>> GetProductByIdAsync(long id);

        /// <summary>
        /// Получить список продуктов по фильтру
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <returns>Результат со списоком продуктов</returns>
        Task<IResult<ProductListResult>> GetProductListAsync(ProductListFilter filter);

        /// <summary>
        /// Добавить новый товар
        /// </summary>
        /// <param name="dto">Товар</param>
        /// <returns>Идентификатор добавленного товара</returns>
        Task<IResult<long>> AddProductAsync(ProductDto dto);
        
        /// <summary>
        /// Обновить товар
        /// </summary>
        /// <param name="dto">Товар</param>
        Task<IResult> UpdateProductAsync(ProductDto dto);
        
        /// <summary>
        /// Удалить товар
        /// </summary>
        /// <param name="id">Идентификатор продукта</param>
        Task<IResult> DeleteProductAsync(long id);
    }
}