using System.Threading.Tasks;

namespace Shop.Service
{
    /// <summary>
    /// Сервис кэширования запросов
    /// </summary>
    public interface IRequestCachingService
    {
        /// <summary>
        /// Попытаться получить результат
        /// </summary>
        /// <param name="actionKey">Ключ секции</param>
        /// <param name="requestKey">Ключ запроса</param>
        /// <returns></returns>
        ValueTask<object> TryGetResultAsync(string actionKey, string requestKey);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionKey">Ключ секции</param>
        /// <param name="requestKey">Ключ запроса</param>
        /// <param name="actionResult">Сохраняемых результат</param>
        /// <returns></returns>
        ValueTask AddResultAsync(string actionKey, string requestKey, object actionResult);

        /// <summary>
        /// Очистить секции запросов
        /// </summary>
        /// <param name="actionKeys">Ключи секций</param>
        /// <returns></returns>
        ValueTask ClearAsync(params string[] actionKeys);
    }
}