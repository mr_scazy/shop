using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Shop.Core.Entities;
using Shop.Core.Enums;
using Shop.Core.Results;
using Shop.Data;
using Shop.Service.DTO;
using Shop.Service.Extensions;

namespace Shop.Service.Impl
{
    /// <inheritdoc cref="IProductService"/>
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _uow;

        private readonly ILogger<ProductService> _logger;

        private const string ProductNotFoundMsg = "Позиция товара не найдена";

        /// <summary>
        /// Ctor
        /// </summary>
        public ProductService(IUnitOfWork uow, ILogger<ProductService> logger)
        {
            _uow = uow;
            _logger = logger;
        }

        /// <inheritdoc cref="IProductService.GetProductByIdAsync"/>
        public async Task<IResult<ProductDto>> GetProductByIdAsync(long id)
        {
            var entity = await _uow.Set<Product>()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
            
            if (entity == null)
            {
                const ErrorCodes errorCode = ErrorCodes.BusinessError;
                
                _logger.LogWarning($"{ProductNotFoundMsg}: errorCode={errorCode}");
                
                return Result.Failed<ProductDto>(errorCode, ProductNotFoundMsg);
            }

            var result = entity.ToDto();

            return Result.FromData(result);
        }

        /// <inheritdoc cref="IProductService.GetProductListAsync"/>
        public async Task<IResult<ProductListResult>> GetProductListAsync(ProductListFilter filter)
        {
            var query = _uow.Set<Product>()
                .AsNoTracking()
                .OrderByDescending(x => x.CreatedDate);

            var take = filter.Take;
            var skip = (filter.Page - 1) * take;
            
            var items = await query
                .Select(x => new ProductDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    Price = x.Price,
                    CreatedDate = x.CreatedDate
                })
                .Skip(skip)
                .Take(take)
                .ToArrayAsync();

            var total = await query.CountAsync();

            var result = new ProductListResult
            {
                Items = items,
                Total = total
            };

            return Result.FromData(result);
        }

        /// <inheritdoc cref="IProductService.AddProductAsync"/>
        public async Task<IResult<long>> AddProductAsync(ProductDto dto)
        {
            var entity = new Product
            {
                Name = dto.Name,
                Description = dto.Description,
                Price = dto.Price,
                CreatedDate = DateTime.UtcNow
            };
            
            await _uow.AddAsync(entity);
            await _uow.SaveChangesAsync();

            return Result.FromData(entity.Id);
        }

        /// <inheritdoc cref="IProductService.UpdateProductAsync"/>
        public async Task<IResult> UpdateProductAsync(ProductDto dto)
        {
            var entity = await _uow.Set<Product>().FirstOrDefaultAsync(x => x.Id == dto.Id);
            if (entity == null)
            {
                const ErrorCodes errorCode = ErrorCodes.BusinessError;
                
                _logger.LogWarning($"{ProductNotFoundMsg}: errorCode={errorCode}");
                
                return Result.Failed(errorCode, ProductNotFoundMsg);
            }
            
            entity.Name = dto.Name;
            entity.Description = dto.Description;
            entity.Price = dto.Price;
            await _uow.SaveChangesAsync();

            return Result.Succeed();
        }

        /// <inheritdoc cref="IProductService.DeleteProductAsync"/>
        public async Task<IResult> DeleteProductAsync(long id)
        {
            var entity = await _uow.Set<Product>().FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
            {
                const ErrorCodes errorCode = ErrorCodes.BusinessError;
                
                _logger.LogWarning($"{ProductNotFoundMsg}: errorCode={errorCode}");
                
                return Result.Failed(errorCode, ProductNotFoundMsg);
            }
            
            _uow.Remove(entity);
            await _uow.SaveChangesAsync();
            
            return Result.Succeed();
        }
    }
}