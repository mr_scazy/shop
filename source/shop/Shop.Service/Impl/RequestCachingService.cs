using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Shop.Service.Impl
{
    /// <inheritdoc cref="IRequestCachingService"/>
    public class RequestCachingService : IRequestCachingService
    {
        private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, object>> _cache 
            = new ConcurrentDictionary<string, ConcurrentDictionary<string, object>>();

        /// <inheritdoc cref="IRequestCachingService.TryGetResultAsync"/>
        public ValueTask<object> TryGetResultAsync(string actionKey, string requestKey)
        {
            var actionCache = GetActionCache(actionKey);
            return actionCache.TryGetValue(requestKey, out var result)
                ? new ValueTask<object>(result)
                : new ValueTask<object>();
        }

        /// <inheritdoc cref="IRequestCachingService.AddResultAsync"/>
        public ValueTask AddResultAsync(string actionKey, string requestKey, object actionResult)
        {
            var actionCache = GetActionCache(actionKey);

            actionCache.TryAdd(requestKey, actionResult);
            return new ValueTask();
        }

        /// <inheritdoc cref="IRequestCachingService.ClearAsync"/>
        public ValueTask ClearAsync(params string[] actionKeys)
        {
            foreach (var actionKey in actionKeys)
            {
                var actionCache = GetActionCache(actionKey);
                actionCache.Clear();
            }
           
            return new ValueTask();
        }

        private ConcurrentDictionary<string, object> GetActionCache(string actionKey)
        {
            if (_cache.TryGetValue(actionKey, out var actionCache))
            {
                return actionCache;
            }
            
            actionCache = new ConcurrentDictionary<string, object>();

            _cache.TryAdd(actionKey, actionCache);

            return actionCache;
        }
    }
}