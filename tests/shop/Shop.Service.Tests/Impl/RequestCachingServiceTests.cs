using System.Threading.Tasks;
using NUnit.Framework;
using Shop.Service.Impl;

namespace Shop.Service.Tests.Impl
{
    /// <summary>
    /// Тесты для <see cref="RequestCachingService"/>
    /// </summary>
    public class RequestCachingServiceTests
    {
        private RequestCachingService _target;

        [SetUp]
        public void Setup()
        {
            _target = new RequestCachingService();
        }

        [Test]
        public async Task AddResultAsync_GetExpectedResult()
        {
            const string section = "section";
            const string key = "key";
            const string expectedData = "ssss";

            await _target.AddResultAsync(section, key, expectedData);

            var result = await _target.TryGetResultAsync(section, key);
            var data = result as string;

            Assert.True(expectedData == data);
        }
    }
}